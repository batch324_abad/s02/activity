<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity S02</title>
</head>
<body>

	<h2>Divisible of Fives</h2>
	<?php printDivisibleByFive(); ?>

	<h2>Array Manipulation</h2>
	<?php array_unshift($students, 'John Smith'); ?>
	<p><?php print_r($students); ?></p>

	<p><?php echo count($students); ?></p>

	<?php array_push($students, 'Jane Smith'); ?>
	<p><?php print_r($students); ?></p>

	<p><?php echo count($students); ?></p>

	<?php array_shift($students); ?>
	<p><?php print_r($students); ?></p>

	<p><?php echo count($students); ?></p>


</body>
</html>