<?php 

// [SECTION] Repetition Control Structures

	// it is used to execute code multiple times. 

	// While Loop
		// A while loop takes a single condition.

function whileLoop(){
	$count = 5;

	while($count !== 0){
		echo $count.'<br/>';
		// echo $count;
		$count--;
	}
}

// function whileLoop1($count){
// 	$count = 10;

// 	while($count !== 0){
// 		echo $count.'<br/>';
// 		$count--;
// 	}
// }

	// Do-While Loop
		// A do-while loop works a lot like the while loop. The only difference is that, do-while guarantee that the code will run / executed at least once.

function doWhileLoop() {
	$count = 20;

	do{
		echo $count.'<br/>';
		$count--;
	} while ($count > 20);
}

	// For Loop
		/*
			for (initial value: condition; iteration){
				code block
			}
		*/

function forLoop() {
	for($count = 0; $count <= 20; $count++){
		echo $count.'<br/>';
	}
}

	// Continue and Break Statement
	
		// "Continue" is a keyword that allows the code to go to the next loop without finish the current code block
		// "Break" on the other hand is a keyword that stop the execution of the current loop.

function modifiedForLoop() {
	// 1st count is where to start; 2nd count is where to end; 3rd count what to do
	for ($count = 0; $count <= 20; $count++){
		// % or modulo is used to check the remainder
		// If the count is divisible by 2, do this:
		if ($count % 2 === 0){
			continue;
		}
		// If not just continue the iteration
		echo $count.'<br/>';

		// If the count is greater than 10, do this:
		if($count > 10) {
			break;
		}
	}
}

// [SECTION] Array Manipulation
	
	// An array is a kind of variable that can hold more than one value
	// Arrays are declared using array() function or square brackets '[]'.
		// In the earlier version of php, we cannot use [], but as of Php 5.4 we can use the short array syntax which replace array() with [].

// Before the Php 5.4
$studentNumbers = array('2020-1923', '2020-1924', '2020-1965', '2020-1926', '2020-1927');

// After the Php 5.4
$studentNumbers = ['2020-1923', '2020-1924', '2020-1965', '2020-1926', '2020-1927'];

// Simple Arrays
$grades = [98.5, 94.3, 89.2, 90.1];
$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];
$tasks = [
	'drink html',
	'eat php',
	'inhale css',
	'bake javascript'
	];

// Associative Array
	// Associative Array differ from numeric array in the sense that associative array uses descritptive names in naming the elements/values (key=>value pair).
	// Double Arrow Operator(=>) - an assignemnt operator that is commonly used in the creation of associative array.

$gradePeriods = ['firstGrading' => 98.5, 'secondGrading' => 94.3, 'thirdGrading' => 89.2, 'fourthGrading' => 90.1];

// Two-Dimensional Array
	// Two dimensional array is commonly used in image processing, good example of this is our viewing screen that uses multidimensional array of pixels. 

$heroes = [
	['iron man', 'thor', 'hulk'],
	['wolverine', 'cyclops', 'jean grey'],
	['batman', 'superman', 'wonder woman']
];

// Two-Dimensional Associative Array

$ironManPowers = [
	'regular' => ['repulsor blast', 'rocket punch'],
	'signature' => ['unibeam']
];

// Array Iterative Method: foreach();
// $heroes is the name of array while $hero is the name of each element
// foreach($heroes as $hero){
// 	print_r($hero);
// }

// [Section] Array Mutators
// Array Mutators modify the contents of an array.

// Array Sorting

$sortedBrands = $computerBrands;
$reverseSortedBrands = $computerBrands;

// sort alphabetically
sort($sortedBrands);
rsort($reverseSortedBrands);